import pandas as pd
import numpy as np
import math
import os
#____________________________________________________________________________________________________________________________________
#reading the data
DATA_VERSION = 1.0
FILEPATH = "/shared/DataSets/SRTR1609_Kidney"
save_filename = os.path.join(FILEPATH, f"hla_preprocessing/Merged_Data_Preprocessing_With_Mods_{DATA_VERSION}.csv")
merged_df = pd.read_csv(save_filename)
#____________________________________________________________________________________________________________________________________

# Choosing the features
reduced_dataset = merged_df[['DON_AGE','REC_AGE_IN_MONTHS_AT_TX','DON_RACE','CAN_RACE','DON_GENDER','REC_MM_EQUIV_TX','CAN_GENDER',
                              'DY_TXFL', 'Death_Censor_dates', 'GL_Censor_dates']]

#____________________________________________________________________________________________________________________________________

# eliminating rows with missing data
index = ~reduced_dataset['DON_AGE'].isna()
reduced_dataset = reduced_dataset[index]
len(reduced_dataset)
"""
samples reduced from 400888 to 400871
"""
index = ~reduced_dataset['REC_AGE_IN_MONTHS_AT_TX'].isna()
reduced_dataset = reduced_dataset[index]
len(reduced_dataset)
"""
samples reduced from 400871 to 400870
"""
index = ~reduced_dataset['DON_RACE'].isna()
reduced_dataset = reduced_dataset[index]
len(reduced_dataset)
"""
samples reduced from 400870 to 400793
"""

index = ~reduced_dataset['CAN_RACE'].isna()
reduced_dataset = reduced_dataset[index]
len(reduced_dataset)
"""
samples reduced from 400793 to 399465
"""

index = ~reduced_dataset['DON_GENDER'].isna()
reduced_dataset = reduced_dataset[index]
len(reduced_dataset)
"""
samples reduced from 399465 to 399465
"""

index = ~reduced_dataset['REC_MM_EQUIV_TX'].isna()
reduced_dataset = reduced_dataset[index]
len(reduced_dataset)
"""
samples reduced from 399465 to 361263
"""

index = ~reduced_dataset['CAN_GENDER'].isna()
reduced_dataset = reduced_dataset[index]
len(reduced_dataset)
"""
number of samples reduced from 361263 to 361263
"""

#____________________________________________________________________________________________________________________________________
# calculating the donor age in months
reduced_dataset['DON_AGE'] = reduced_dataset['DON_AGE'] * 12
#____________________________________________________________________________________________________________________________________
# eliminating samples where we have no data of all these variables together: DY_TXFL, Death_Censor_dates, GL_Censor_dates
DY_TXFL_index = ~reduced_dataset['DY_TXFL'].isna()
Death_Censor_dates_index = ~reduced_dataset['Death_Censor_dates'].isna()
GL_Censor_dates_index = ~reduced_dataset['GL_Censor_dates'].isna()
index = DY_TXFL_index | Death_Censor_dates_index | GL_Censor_dates_index

reduced_dataset = reduced_dataset[index]
'''
Number of samples reduced from 361263 to 287896
'''
#__________________________________________________________________________________________________________________________________
# save reduced dataset
file_path = "/nethome/mnemati/mnemati_datasets/reduced_dataset.csv"
reduced_dataset.to_csv(file_path)
#__________________________________________________________________________________________________________________________________
#Read Dataset
reduced_dataset = pd.read_csv(file_path)
#__________________________________________________________________________________________________________________________________

# censored and uncensored survival in days
uncensored_index = ~reduced_dataset['DY_TXFL'].isna()
index = uncensored_index
uncensored_survival_in_days = reduced_dataset['DY_TXFL'][uncensored_index]
censored_index = ~index
censored_survival_in_days = reduced_dataset['GL_Censor_dates'][censored_index]
frames = [uncensored_survival_in_days, censored_survival_in_days]
survival_in_days = pd.concat(frames).sort_index()
#__________________________________________________________________________________________________________________________________

#creating structured label consisting survival days and if the sample is cencored or not
frm = {'status': uncensored_index, 'survival_in_days' : survival_in_days}
data_y_unstructured = pd.DataFrame(data=frm)

s = data_y_unstructured.dtypes
data_y = np.array([tuple(x) for x in data_y_unstructured.values], dtype=list(zip(s.index, s)))

data_x_1 = reduced_dataset[['DON_AGE', 'REC_AGE_IN_MONTHS_AT_TX', 'DON_RACE','CAN_RACE',
                          'DON_GENDER', 'CAN_GENDER', 'REC_MM_EQUIV_TX']]
#____________________________________________________________________________________________________________________________

# converting race and gender to categories and create a compatible ready to use dataset for sksurv
DON_RACE_CATEGORIZED = data_x_1['DON_RACE'].astype('category')
CAN_RACE_CATEGORIZED = data_x_1['CAN_RACE'].astype('category')
DON_GENDER_CATEGORIZED = data_x_1['DON_GENDER'].astype('category')
CAN_GENDER_CATEGORIZED = data_x_1['CAN_GENDER'].astype('category')

frame = {'DON_AGE': data_x_1['DON_AGE'], 'CAN_AGE': data_x_1['REC_AGE_IN_MONTHS_AT_TX'],
    'DON_RACE': DON_RACE_CATEGORIZED, 'CAN_RACE' : CAN_RACE_CATEGORIZED,
         'DON_GENDER' :DON_GENDER_CATEGORIZED, 'CAN_GENDER' : CAN_GENDER_CATEGORIZED,
         'MM' : data_x_1['REC_MM_EQUIV_TX']}

data_x = pd.DataFrame(data=frame)